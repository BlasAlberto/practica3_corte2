//FUNCIONES
function calcular(valor, plan){
    if(valor != 0 || valor != ""){
        let pEnganche = document.getElementById("enganche");
        let pTotal = document.getElementById("totalFinanciar");
        let pMensualidad = document.getElementById("pagoMensual");
        let lblValor = document.getElementById("lblValorAutomovil");

        lblValor.style.color = "Black";
    
        let enganche = valor * 0.3;
        let interes = 0.0;
        let total = valor - enganche;
        switch(plan){
            case "12":
                interes = total * 0.125;
                break;
            case "18":
                interes = total * 0.172;
                break;
            case "24":
                interes = total * 0.21;
                break;
            case "36":
                interes = total * 0.26;
                break;
            case "48":
                interes = total * 0.45;
                break;
        }
        total += interes; 
        
    
        pEnganche.innerHTML = "$" + enganche.toFixed(2);
        pTotal.innerHTML = "$" + total.toFixed(2);
        pMensualidad.innerHTML = "$" + (total / plan).toFixed(2);
    }
    else{
        let lblValor = document.getElementById("lblValorAutomovil");
        lblValor.style.color = "Red";
        alert("Falta el valor del automovil.");
    }
}

function limpiar(){
    let inputValor = document.getElementById("valorAutomovil");
    let selectPlanes = document.getElementById("planes");
    let pEnganche = document.getElementById("enganche");
    let pTotal = document.getElementById("totalFinanciar");
    let pMensualidad = document.getElementById("pagoMensual");

    inputValor.value = "";
    selectPlanes.value = "12";
    pEnganche.innerHTML = "$0";
    pTotal.innerHTML = "$0";
    pMensualidad.innerHTML = "$0";
}